/*
  HCSR04 - Library for arduino, for HC-SR04 ultrasonic distance sensor.
  Created by Martin Sosic, June 11, 2016.
*/

/* 

If you do not have an LCD library, follow the instructions here : xxxxxxxxxxxxxxxxxxxxx

*/

#include <HCSR04.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display
/*
   TRIG : 12
   Echo : 13
*/
UltraSonicDistanceSensor distanceSensor(13, 12);  // Initialize sensor that uses digital pins 13 and 12.

void setup()
{
  lcd.init();
  lcd.backlight();
  lcd.setCursor(4, 0);
  lcd.print("Hshop.vn");
}


void loop()
{
  //  Do a measurement using the sensor and print the distance in centimeters.
  
  float distance = distanceSensor.measureDistanceCm();
   lcd.setCursor(0, 1);
  lcd.print("Distance: ");
  if (distance < 10)
  {
    lcd.print(distance);
    lcd.println("  ");
  } else if (distance > 10 & distance < 100)
  {
    lcd.print(distance);
    lcd.println(" ");
  } else {
    lcd.print("*****");
  }
  delay(100);
}